import java.util.HashMap;

public class numero02 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<String, Integer>();
        personas.put("Juan", 25);
        personas.put("María", 30);
        personas.put("Pedro", 42);
        personas.put("Luisa", 19);
        
       
        System.out.println("Lista de personas y edades: " + personas);
    }
}
