import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class numero07 {
    public static void main(String[] args) {
        String rutaArchivo = "C:\\Users\\Usuario\\Desktop\\ejemplo.txt";
        try {
            BufferedReader br = new BufferedReader(new FileReader(rutaArchivo));
            String linea;
            while ((linea = br.readLine()) != null) {
                System.out.println(linea);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
