import java.text.*;
import java.util.*;

public class numero12 {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

        String fechaFormateada = formatoFecha.format(fechaActual);
        System.out.println("La fecha actual es: " + fechaFormateada);
    }
}
