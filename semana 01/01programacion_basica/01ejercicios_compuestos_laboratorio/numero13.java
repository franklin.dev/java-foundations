import java.text.*;
import java.util.*;

public class numero13 {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        String fechaFormateada = formatoFecha.format(fechaActual);
        System.out.println("La fecha y hora actual es: " + fechaFormateada);
    }
}
