import java.math.*;

public class numero14 {
    public static void main(String[] args) {
        BigDecimal num1 = new BigDecimal("12345678901234567890.12345");
        BigDecimal num2 = new BigDecimal("98765432109876543210.54321");

        BigDecimal suma = num1.add(num2);
        BigDecimal resta = num2.subtract(num1);
        BigDecimal multiplicacion = num1.multiply(num2);
        BigDecimal division = num2.divide(num1, 10, RoundingMode.HALF_UP);

        System.out.println("La suma es: " + suma);
        System.out.println("La resta es: " + resta);
        System.out.println("La multiplicación es: " + multiplicacion);
        System.out.println("La división es: " + division);
    }
}
