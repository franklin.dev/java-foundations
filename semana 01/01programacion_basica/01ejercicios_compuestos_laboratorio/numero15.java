import java.math.*;

public class numero15 {
    public static void main(String[] args) {
        BigInteger num1 = new BigInteger("12345678901234567890");
        BigInteger num2 = new BigInteger("98765432109876543210");

        BigInteger suma = num1.add(num2);
        BigInteger resta = num2.subtract(num1);
        BigInteger multiplicacion = num1.multiply(num2);
        BigInteger division = num2.divide(num1);

        System.out.println("La suma es: " + suma);
        System.out.println("La resta es: " + resta);
        System.out.println("La multiplicación es: " + multiplicacion);
        System.out.println("La división es: " + division);
    }
}