import java.util.*;

public class numero16 {
    public static void main(String[] args) {
        TimeZone zonaHoraria = TimeZone.getTimeZone("America/Mexico_City");
        Calendar calendario = Calendar.getInstance(zonaHoraria);
        
        System.out.println("Fecha y hora actual en " + zonaHoraria.getDisplayName() + ": " + calendario.getTime());
    }
}