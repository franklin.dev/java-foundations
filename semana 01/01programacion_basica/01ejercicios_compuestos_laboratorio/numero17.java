import java.util.*;

public class numero17 {
    public static void main(String[] args) {
        Locale configuracionRegional = new Locale("es", "MX");
        DateFormat formatoFecha = DateFormat.getDateInstance(DateFormat.SHORT, configuracionRegional);
        
        System.out.println("Fecha actual en " + configuracionRegional.getDisplayName() + ": " + formatoFecha.format(new Date()));
    }
}
