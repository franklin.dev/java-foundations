
import java.util.Scanner;

public class saludar {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        // Solicitar al usuario que ingrese su nombre
        System.out.print("Ingresa tu nombre: ");
        String nombre = sc.nextLine();

        // Saludar al usuario utilizando su nombre
        System.out.println("Hola, " + nombre + "!");

        sc.close();
    }
}
