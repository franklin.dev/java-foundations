import java.util.Scanner;

public class ejercicio03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese dos números enteros
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = scanner.nextInt();
        
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = scanner.nextInt();
        
        // Sumar los dos números y guardar el resultado en una variable
        int suma = numero1 + numero2;
        
        // Imprimir el resultado de la suma en la pantalla
        System.out.println("La suma de " + numero1 + " y " + numero2 + " es igual a " + suma);
        
        scanner.close();
    }
}
