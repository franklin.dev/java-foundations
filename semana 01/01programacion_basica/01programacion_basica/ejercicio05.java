import java.util.Scanner;

public class ejercicio05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese dos números enteros
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = scanner.nextInt();
        
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = scanner.nextInt();
        
        // Calcular la diferencia entre los dos números y guardar el resultado en una variable
        int resultado = numero1 - numero2;
        
        // Imprimir el resultado de la resta en la pantalla
        System.out.println("El resultado de la resta de " + numero1 + " y " + numero2 + " es igual a " + resultado);
        
        scanner.close();
    }
}

