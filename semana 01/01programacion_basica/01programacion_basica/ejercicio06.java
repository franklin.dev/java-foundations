import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese dos números enteros
        System.out.print("Ingrese el dividendo (un número entero): ");
        int dividendo = scanner.nextInt();
        
        System.out.print("Ingrese el divisor (un número entero): ");
        int divisor = scanner.nextInt();
        
        // Calcular el cociente y el resto de la división y guardar los resultados en variables
        int cociente = dividendo / divisor;
        int resto = dividendo % divisor;
        
        // Imprimir el resultado en la pantalla
        System.out.println("El cociente de la división es: " + cociente);
        System.out.println("El resto de la división es: " + resto);
        
        scanner.close();
    }
}

