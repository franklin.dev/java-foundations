import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese la base y la altura del triángulo
        System.out.print("Ingrese la base del triángulo: ");
        double base = scanner.nextDouble();
        
        System.out.print("Ingrese la altura del triángulo: ");
        double altura = scanner.nextDouble();
        
        // Calcular el área del triángulo y guardar el resultado en una variable
        double area = (base * altura) / 2;
        
        // Imprimir el resultado en la pantalla
        System.out.println("El área del triángulo es: " + area);
        
        scanner.close();
    }
}

