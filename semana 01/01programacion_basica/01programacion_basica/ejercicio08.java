import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese la longitud de los lados del triángulo
        System.out.print("Ingrese la longitud del lado 1: ");
        double lado1 = scanner.nextDouble();
        
        System.out.print("Ingrese la longitud del lado 2: ");
        double lado2 = scanner.nextDouble();
        
        System.out.print("Ingrese la longitud del lado 3: ");
        double lado3 = scanner.nextDouble();
        
        // Verificar si el triángulo es equilátero
        if (lado1 == lado2 && lado2 == lado3) {
            System.out.println("El triángulo es equilátero");
        }
        // Verificar si el triángulo es isósceles
        else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            System.out.println("El triángulo es isósceles");
        }
        // Si no es equilátero ni isósceles, entonces es escaleno
        else {
            System.out.println("El triángulo es escaleno");
        }
        
        scanner.close();
    }
}
