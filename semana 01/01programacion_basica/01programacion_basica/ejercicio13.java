import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Solicitar al usuario que ingrese una cadena de texto
        System.out.print("Ingrese una cadena de texto: ");
        String cadena = scanner.nextLine();
        
        // Imprimir la cadena de texto ingresada por el usuario
        System.out.println("La cadena de texto ingresada es: " + cadena);
        
        scanner.close();
    }
}

