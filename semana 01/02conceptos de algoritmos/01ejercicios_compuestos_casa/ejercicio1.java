public class ejercicio1 {
    public static void main(String[] args) {
        int[] numeros = {2, 4, 6, 8, 10}; 
        int sumaObjetivo = 12; 

        boolean encontrado = false; 
        for (int i = 0; i < numeros.length; i++) {
            for (int j = i + 1; j < numeros.length; j++) {
                if (numeros[i] + numeros[j] == sumaObjetivo) {
                    System.out.println("Los números " + numeros[i] + " y " + numeros[j] + " suman " + sumaObjetivo);
                    encontrado = true;
                }
            }
        }

        if (!encontrado) {
            System.out.println("No se encontró ningún par de números que sumen " + sumaObjetivo);
        }
    }
}
