public class ejercicio10 {
    public static void main(String[] args) {
        int maximo = 1000000;
        int contador = 0;
        for (int i = 1; i <= maximo; i++) {
            int sumaFactoriales = 0;
            int numero = i;
            while (numero > 0) {
                int digito = numero % 10;
                sumaFactoriales += factorial(digito);
                numero /= 10;
            }
            if (sumaFactoriales == i) {
                System.out.println(i + " cumple con la propiedad");
                contador++;
            }
        }
        System.out.println("Se encontraron " + contador + " números que cumplen con la propiedad");
    }

    private static int factorial(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}
