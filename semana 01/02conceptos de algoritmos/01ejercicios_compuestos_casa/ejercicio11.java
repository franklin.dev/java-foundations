import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ejercicio11 {

    public static void main(String[] args) {
        String archivo = "palabras.txt"; // archivo que contiene las palabras
        List<String> palabras = new ArrayList<>(); // lista para almacenar las palabras que cumplen con la propiedad

        try (BufferedReader br = new BufferedReader(new FileReader(archivo))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                String[] palabrasLinea = linea.split(" "); // separar las palabras en la línea
                for (String palabra : palabrasLinea) {
                    if (palabra.length() >= 4 && palabra.charAt(0) == palabra.charAt(3)) {
                        palabras.add(palabra);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Las siguientes palabras tienen la misma letra en las posiciones 1 y 4:");
        for (String palabra : palabras) {
            System.out.println(palabra);
        }
    }
}
