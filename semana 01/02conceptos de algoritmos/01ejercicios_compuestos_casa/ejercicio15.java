public class ejercicio15 {

    public static void main(String[] args) {
        String[] palabras = {"casa", "perro", "sol", "anaconda", "oso", "huevo"};
        int contador = 0;

        for (String palabra : palabras) {
            if (palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
                System.out.println(palabra + " tiene la misma letra al principio y al final.");
                contador++;
            }
        }

        System.out.println("Encontradas " + contador + " palabras con la misma letra al principio y al final.");
    }
}
