public class ejercicio16 {

    public static void main(String[] args) {
        int rangoInicio = 100;
        int rangoFin = 999;
        int contador = 0;

        for (int numero = rangoInicio; numero <= rangoFin; numero++) {
            int suma = 0;
            int n = numero;

            while (n != 0) {
                int digito = n % 10;
                suma += Math.pow(digito, String.valueOf(numero).length());
                n /= 10;
            }

            if (suma == numero) {
                System.out.println(numero + " es un número de Armstrong.");
                contador++;
            }
        }

        System.out.println("Encontrados " + contador + " números de Armstrong.");
    }
}
