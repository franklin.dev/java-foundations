public class ejercicio20 {

    public static void main(String[] args) {
        int inicio = 1;
        int fin = 10000;

        int contador = 0;
        for (int i = inicio; i <= fin; i++) {
            int numero = i;
            int cantidadDeDigitos = contarDigitos(numero);
            int sumaDePotencias = 0;

            while (numero > 0) {
                int digito = numero % 10;
                sumaDePotencias += Math.pow(digito, cantidadDeDigitos);
                numero /= 10;
            }

            if (sumaDePotencias == i) {
                System.out.println(i + " es un número de narcisismo.");
                contador++;
            }
        }

        System.out.println("Hay " + contador + " números de narcisismo en el rango de " + inicio + " a " + fin + ".");
    }

    private static int contarDigitos(int numero) {
        int cantidadDeDigitos = 0;

        while (numero > 0) {
            cantidadDeDigitos++;
            numero /= 10;
        }

        return cantidadDeDigitos;
    }
}
