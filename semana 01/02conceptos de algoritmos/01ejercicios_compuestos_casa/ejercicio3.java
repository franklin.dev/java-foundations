public class ejercicio3 {
    public static void main(String[] args) {
        int inicio = 1; // primer número del rango
        int fin = 100; // último número del rango

        int contadorPerfectos = 0; // contador de números perfectos
        int contadorFibonacci = 0; // contador de números de Fibonacci

        for (int i = inicio; i <= fin; i++) {
            if (esNumeroPerfecto(i)) {
                contadorPerfectos++;
            }
            if (esNumeroFibonacci(i)) {
                contadorFibonacci++;
            }
        }

        System.out.println("Hay " + contadorPerfectos + " números perfectos en el rango del " + inicio + " al " + fin);
        System.out.println("Hay " + contadorFibonacci + " números de Fibonacci en el rango del " + inicio + " al " + fin);
    }
  
    public static boolean esNumeroPerfecto(int n) {
        int suma = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                suma += i;
            }
        }
        return suma == n;
    }
    public static boolean esNumeroFibonacci(int n) {
        if (n == 0 || n == 1) {
            return true;
        }
        int a = 0;
        int b = 1;
        int c = a + b;
        while (c <= n) {
            if (c == n) {
                return true;
            }
            a = b;
            b = c;
            c = a + b;
        }
        return false;
    }
}
