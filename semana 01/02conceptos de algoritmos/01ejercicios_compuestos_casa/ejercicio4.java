public class ejercicio4 {
    public static void main(String[] args) {
        int inicio = 1; // primer número del rango
        int fin = 1000; // último número del rango

        int contador = 0; // contador de números divisibles por 1 a 10

        for (int i = inicio; i <= fin; i++) {
            if (esDivisiblePorTodos(i)) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " números divisibles por todos los números del 1 al 10 en el rango del " + inicio + " al " + fin);
    }

    // Verificar si un número es divisible por todos los números del 1 al 10
    public static boolean esDivisiblePorTodos(int n) {
        for (int i = 1; i <= 10; i++) {
            if (n % i != 0) {
                return false;
            }
        }
        return true;
    }
}
