import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese una cadena de texto: ");
        String texto = scanner.nextLine();
        scanner.close();

        String[] palabras = texto.split("\\s+"); // dividir el texto en palabras

        for (String palabra : palabras) {
            if (tieneTodasLasVocales(palabra)) {
                System.out.println(palabra + " contiene todas las vocales.");
            }
        }
    }

    
    public static boolean tieneTodasLasVocales(String palabra) {
        return palabra.matches("(?i).*a.*e.*i.*o.*u.*");
    }
}
