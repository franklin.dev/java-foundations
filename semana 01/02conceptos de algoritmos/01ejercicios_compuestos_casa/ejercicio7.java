import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio7 {
    public static void main(String[] args) {
        String texto = "Hay algunas palabras como hoooooooola o aggggggggh que tienen letras repetidas tres veces consecutivas";
        Pattern patron = Pattern.compile("\\b(\\w*(\\w)\\2{2}\\w*)\\b");
        Matcher matcher = patron.matcher(texto);
        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }
    }
}
