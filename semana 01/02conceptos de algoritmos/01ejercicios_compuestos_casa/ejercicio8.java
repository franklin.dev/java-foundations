public class ejercicio8 {
    public static void main(String[] args) {
        int sumaDigitos = 0;
        for (int i = 1; i <= 100; i++) {
            int numero = i;
            int suma = 0;
            while (numero > 0) {
                suma += numero % 10;
                numero /= 10;
            }
            sumaDigitos += suma;
        }
        for (int i = 1; i <= 100; i++) {
            if (i % sumaDigitos == 0) {
                System.out.println(i + " es divisible por la suma de los digitos de 1 a 100");
            }
        }
    }
}
