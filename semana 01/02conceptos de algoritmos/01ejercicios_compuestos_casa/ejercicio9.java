import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio9 {
    public static void main(String[] args) {
        String texto = "En este texto hay algunas palabras con letras repetidas un número impar de veces como aaaah, loooool, o taaantos";
        Pattern patron = Pattern.compile("\\b(\\w*(\\w)(?:\\w*\\2){1}\\w*)\\b");
        Matcher matcher = patron.matcher(texto);
        int contador = 0;
        while (matcher.find()) {
            String palabra = matcher.group(1);
            char letraRepetida = matcher.group(2).charAt(0);
            int cantidadRepeticiones = contarRepeticiones(palabra, letraRepetida);
            if (cantidadRepeticiones % 2 == 1) {
                System.out.println(palabra + " tiene una letra repetida un número impar de veces");
                contador++;
            }
        }
        System.out.println("Se encontraron " + contador + " palabras con una letra repetida un número impar de veces");
    }

    private static int contarRepeticiones(String palabra, char letra) {
        int contador = 0;
        for (char c : palabra.toCharArray()) {
            if (c == letra) {
                contador++;
            }
        }
        return contador;
    }
}
