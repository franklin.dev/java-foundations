import java.util.Scanner;

public class ejercicio01 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingrese un número entero: ");
      int num = scanner.nextInt();

      if (num % 2 == 0) {
         System.out.println(num + " es un número par");
      } else {
         System.out.println(num + " es un número impar");
      }
   }
}
