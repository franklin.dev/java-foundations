import java.util.Scanner;

public class ejercicio02 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingrese un número entero: ");
      int num = scanner.nextInt();

      if (num > 0) {
         System.out.println(num + " es un número positivo");
      } else if (num < 0) {
         System.out.println(num + " es un número negativo");
      } else {
         System.out.println(num + " es cero");
      }
   }
}
