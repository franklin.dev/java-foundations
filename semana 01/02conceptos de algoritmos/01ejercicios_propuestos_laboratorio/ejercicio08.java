import java.util.Scanner;

public class FechaValida {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingrese una fecha en formato dd/mm/yyyy: ");
      String fecha = scanner.nextLine();

      // Dividimos la cadena de la fecha en día, mes y año utilizando el separador "/"
      String[] partes = fecha.split("/");
      int dia = Integer.parseInt(partes[0]);
      int mes = Integer.parseInt(partes[1]);
      int anio = Integer.parseInt(partes[2]);

      boolean esFechaValida = true;

      // Comprobamos si el año es bisiesto
      boolean esBisiesto = false;
      if (anio % 4 == 0) {
         if (anio % 100 == 0) {
            if (anio % 400 == 0) {
               esBisiesto = true;
            }
         } else {
            esBisiesto = true;
         }
      }

      // Comprobamos si el mes es válido
      if (mes < 1 || mes > 12) {
         esFechaValida = false;
      }

      // Comprobamos si el día es válido
      switch (mes) {
         case 2:
            if (esBisiesto) {
               if (dia < 1 || dia > 29) {
                  esFechaValida = false;
               }
            } else {
               if (dia < 1 || dia > 28) {
                  esFechaValida = false;
               }
            }
            break;
         case 4:
         case 6:
         case 9:
         case 11:
            if (dia < 1 || dia > 30) {
               esFechaValida = false;
            }
            break;
         default:
            if (dia < 1 || dia > 31) {
               esFechaValida = false;
            }
      }

      if (esFechaValida) {
         System.out.println("La fecha es válida");
      } else {
         System.out.println("La fecha no es válida");
      }
   }
}
