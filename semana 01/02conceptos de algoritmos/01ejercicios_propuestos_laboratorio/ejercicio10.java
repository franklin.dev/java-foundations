import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = scanner.nextLine();
        
        String[] numerosStr = input.split(" ");
        int contadorPares = 0;
        for (String numeroStr : numerosStr) {
            int numero = Integer.parseInt(numeroStr);
            if (numero % 2 == 0) {
                contadorPares++;
            }
        }
        
        System.out.println("La cantidad de números pares es: " + contadorPares);
    }
}
