import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = scanner.nextLine();
        
        String[] numerosStr = input.split(" ");
        int contadorNumerosPerfectos = 0;
        for (String numeroStr : numerosStr) {
            int numero = Integer.parseInt(numeroStr);
            if (esNumeroPerfecto(numero)) {
                contadorNumerosPerfectos++;
            }
        }
        
        System.out.println("La cantidad de números perfectos es: " + contadorNumerosPerfectos);
    }
    
    private static boolean esNumeroPerfecto(int numero) {
        int sumaDivisores = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == numero;
    }
}
