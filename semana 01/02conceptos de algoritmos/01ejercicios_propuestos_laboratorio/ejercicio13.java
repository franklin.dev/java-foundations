import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = scanner.nextLine();
        
        String[] numerosStr = input.split(" ");
        int contadorNumerosFibonacci = 0;
        for (String numeroStr : numerosStr) {
            int numero = Integer.parseInt(numeroStr);
            if (esNumeroDeFibonacci(numero)) {
                contadorNumerosFibonacci++;
            }
        }
        
        System.out.println("La cantidad de números de Fibonacci es: " + contadorNumerosFibonacci);
    }
    
    private static boolean esNumeroDeFibonacci(int numero) {
        int a = 0, b = 1;
        while (b < numero) {
            int temp = b;
            b += a;
            a = temp;
        }
        return b == numero;
    }
}
