import java.util.Scanner;

public class ejercicio15 {

    public static void main(String[] args) {
        
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);
        
        // Pedir al usuario que ingrese una lista de palabras separadas por espacios
        System.out.print("Ingrese una lista de palabras separadas por espacios: ");
        String listaPalabras = scanner.nextLine();
        
        // Convertir la lista de palabras en un arreglo
        String[] palabras = listaPalabras.split(" ");
        
        // Contar cuántas palabras comienzan con la letra "a"
        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.toLowerCase().startsWith("a")) {
                contador++;
            }
        }
        
        // Mostrar el resultado al usuario
        System.out.println("Hay " + contador + " palabras que comienzan con la letra 'a'.");
    }

}
