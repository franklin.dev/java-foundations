import java.util.Scanner;

public class ejercicio16 {

    public static void main(String[] args) {
        
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);
        
        // Pedir al usuario que ingrese una lista de palabras separadas por espacios
        System.out.print("Ingrese una lista de palabras separadas por espacios: ");
        String listaPalabras = scanner.nextLine();
        
        // Convertir la lista de palabras en un arreglo
        String[] palabras = listaPalabras.split(" ");
        
        // Buscar palabras que contengan la letra "e"
        System.out.println("Las siguientes palabras contienen la letra 'e':");
        for (String palabra : palabras) {
            if (palabra.toLowerCase().contains("e")) {
                System.out.println(palabra);
            }
        }
    }

}
