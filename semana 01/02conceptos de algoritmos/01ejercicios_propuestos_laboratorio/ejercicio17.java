import java.util.Scanner;

public class ejercicio17 {

    public static void main(String[] args) {
        
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner scanner = new Scanner(System.in);
        
        // Pedir al usuario que ingrese una lista de palabras separadas por espacios
        System.out.print("Ingrese una lista de palabras separadas por espacios: ");
        String listaPalabras = scanner.nextLine();
        
        // Pedir al usuario que ingrese el número mínimo de caracteres que deben tener los factores
        System.out.print("Ingrese el número mínimo de caracteres que deben tener los factores: ");
        int longitudMinima = scanner.nextInt();
        
        // Convertir la lista de palabras en un arreglo
        String[] palabras = listaPalabras.split(" ");
        
        // Buscar factores que tengan una longitud mayor que longitudMinima
        System.out.println("Los siguientes factores tienen una longitud mayor que " + longitudMinima + ":");
        for (String palabra : palabras) {
            for (int i = longitudMinima; i <= palabra.length(); i++) {
                String factor = palabra.substring(0, i);
                if (factor.length() >= longitudMinima) {
                    System.out.println(factor);
                }
            }
        }
    }

}
