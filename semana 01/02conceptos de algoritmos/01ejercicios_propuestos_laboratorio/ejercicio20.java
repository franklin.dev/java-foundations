import java.util.Scanner;

public class ejercicio20 {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingrese la lista de números enteros separados por comas: ");
        String input = scanner.nextLine();
        
        System.out.print("Ingrese el número mínimo de divisores requerido: ");
        int minDivisores = scanner.nextInt();
        
        String[] numStrings = input.split(",");
        int[] numeros = new int[numStrings.length];
        for (int i = 0; i < numStrings.length; i++) {
            numeros[i] = Integer.parseInt(numStrings[i].trim());
        }
        
        int cantidad = 0;
        for (int num : numeros) {
            int cantidadDivisores = contarDivisores(num);
            if (cantidadDivisores > minDivisores) {
                cantidad++;
            }
        }
        
        System.out.println("Hay " + cantidad + " números con más de " + minDivisores + " divisores.");
    }
    
    public static int contarDivisores(int n) {
        int cantidad = 0;
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                cantidad++;
            }
        }
        return cantidad;
    }

}
