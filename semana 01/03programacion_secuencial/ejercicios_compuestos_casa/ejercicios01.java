import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" el primer número entero: ");
    int numero1 = scanner.nextInt();
    System.out.print(" el segundo número entero: ");
    int numero2 = scanner.nextInt();
    System.out.println("Los números introducidos son: " + numero1 + " y " + numero2);

    scanner.close();
  }
}
