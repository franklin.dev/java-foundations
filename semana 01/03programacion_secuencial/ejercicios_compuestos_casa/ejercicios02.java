import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" tu nombre: ");
    String nombre = scanner.nextLine();

    System.out.println("Buenos días " + nombre + "!");

    scanner.close();
  }
}
