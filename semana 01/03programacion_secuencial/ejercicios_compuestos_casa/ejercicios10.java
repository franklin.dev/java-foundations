import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" un número de 3 cifras: ");
    int numero = scanner.nextInt();

    int primerDigito = numero / 100;
    int segundoDigito = (numero / 10) % 10;
    int tercerDigito = numero % 10;

    System.out.println("El primer dígito es: " + primerDigito);
    System.out.println("El segundo dígito es: " + segundoDigito);
    System.out.println("El tercer dígito es: " + tercerDigito);

    scanner.close();
  }
}
