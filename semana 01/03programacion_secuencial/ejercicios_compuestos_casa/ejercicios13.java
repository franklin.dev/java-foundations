import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print(" tu fecha de nacimiento (DD/MM/AAAA): ");
    String fechaNacimiento = scanner.nextLine();

    String[] partesFecha = fechaNacimiento.split("/");
    int dia = Integer.parseInt(partesFecha[0]);
    int mes = Integer.parseInt(partesFecha[1]);
    int anio = Integer.parseInt(partesFecha[2]);

    int numeroSuerte = sumaDigitos(dia) + sumaDigitos(mes) + sumaDigitos(anio);

    System.out.println("Tu número de la suerte es: " + numeroSuerte);

    scanner.close();
  }

  public static int sumaDigitos(int numero) {
    int suma = 0;

    while (numero != 0) {
      suma += numero % 10;
      numero /= 10;
    }

    return suma;
  }
}
