import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print(" el número entero N: ");
    int n = scanner.nextInt();
    System.out.print(" el número entero M: ");
    int m = scanner.nextInt();

    int divisor = (int) Math.pow(10, m);

    int resultado = n / divisor;

    System.out.println("El resultado es: " + resultado);

    scanner.close();
  }
}
