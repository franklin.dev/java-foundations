import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Introduce la temperatura en grados centígrados: ");
    double celsius = scanner.nextDouble();
    double kelvin = celsius + 273.15;
    double reamur = celsius * 0.8;
    System.out.println(celsius + " grados centígrados son equivalentes a:");
    System.out.println(kelvin + " grados Kelvin");
    System.out.println(reamur + " grados Reamur");

    scanner.close();
  }
}
