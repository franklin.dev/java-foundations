import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean repeat = true;

        while (repeat) {
            System.out.print("Introduce la temperatura en grados centígrados: ");
            double celsius = scanner.nextDouble();
            double kelvin = celsius + 273.15;
            System.out.println(celsius + " grados centígrados son equivalentes a " + kelvin + " grados Kelvin");
            System.out.print("¿Quieres repetir el proceso? (S/N): ");
            String answer = scanner.next();

            if (answer.equalsIgnoreCase("N")) {
                repeat = false;
            }
        }

        scanner.close();
    }
}
