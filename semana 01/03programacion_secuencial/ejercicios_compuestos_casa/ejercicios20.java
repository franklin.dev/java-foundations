import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce el primer número: ");
        int numero1 = scanner.nextInt();

        System.out.print("Introduce el segundo número: ");
        int numero2 = scanner.nextInt();

        int sumaDivisores1 = 0;
        for (int i = 1; i <= numero1 / 2; i++) {
            if (numero1 % i == 0) {
                sumaDivisores1 += i;
            }
        }

        int sumaDivisores2 = 0;
        for (int i = 1; i <= numero2 / 2; i++) {
            if (numero2 % i == 0) {
                sumaDivisores2 += i;
            }
        }

        if (sumaDivisores1 == numero2 && sumaDivisores2 == numero1) {
            System.out.println(numero1 + " y " + numero2 + " son números amigos.");
        } else {
            System.out.println(numero1 + " y " + numero2 + " no son números amigos.");
        }

        scanner.close();
    }
}
