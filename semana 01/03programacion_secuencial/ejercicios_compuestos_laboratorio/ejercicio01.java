import java.util.Scanner;

public class ejercicio01 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("el eje mayor: ");
        double a = sc.nextDouble();

        System.out.print("el eje menor: ");
        double b = sc.nextDouble();

        double area = Math.PI * a * b;
        double perimetro = 2 * Math.PI * Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);

        System.out.println("Área de la elipse: " + area);
        System.out.println("Perímetro de la elipse: " + perimetro);
    }

}


Ingrese el eje mayor: 5
Ingrese el eje menor: 4
Área de la elipse: 62.83185307179586
Perímetro de la elipse: 28.448331425398703
