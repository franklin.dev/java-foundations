import java.util.Scanner;

public class ejercicio03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("el valor del primer cateto: ");
        double a = sc.nextDouble();

        System.out.print("el valor del segundo cateto: ");
        double b = sc.nextDouble();

        double h = Math.sqrt(a*a + b*b);

        System.out.println("El valor de la hipotenusa es: " + h);
    }
}

 el valor del primer cateto: 5
el valor del segundo cateto: 4
El valor de la hipotenusa es: 6.4031242374328485