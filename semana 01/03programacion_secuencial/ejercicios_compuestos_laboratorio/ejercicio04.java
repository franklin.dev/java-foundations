import java.util.Scanner;

public class ejercicio04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("el valor del primer lado del triángulo: ");
        double a = sc.nextDouble();

        System.out.print("el valor del segundo lado del triángulo: ");
        double b = sc.nextDouble();

        System.out.print("el valor del tercer lado del triángulo: ");
        double c = sc.nextDouble();

        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));

        System.out.println("El área del triángulo es: " + area);
    }
}

el valor del primer lado del triángulo: 5
el valor del segundo lado del triángulo: 7
el valor del tercer lado del triángulo: 6
El área del triángulo es: 14.696938456699069
