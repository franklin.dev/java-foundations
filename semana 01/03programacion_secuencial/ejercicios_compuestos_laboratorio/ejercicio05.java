public class ejercicio05 {
    public static void main(String[] args) {
        double presupuestoTotal = 1000000; // Presupuesto total del hospital
        double presupuestoOncologia = presupuestoTotal * 0.45; // Presupuesto de Oncología (55%)
        double presupuestoPedriatria = presupuestoTotal * 0.25; // Presupuesto de Pediatría (20%)
        double presupuestoTraumatologia = presupuestoTotal * 0.30; // Presupuesto de Traumatología (25%)

        // Mostrar los resultados por consola
        System.out.println("Presupuesto de Oncología: " + presupuestoOncologia);
        System.out.println("Presupuesto de Pediatría: " + presupuestoPedriatria);
        System.out.println("Presupuesto de Traumatología: " + presupuestoTraumatologia);
    }
}

Presupuesto de Oncología: 450000.0
Presupuesto de Pediatría: 250000.0
Presupuesto de Traumatología: 300000.0

