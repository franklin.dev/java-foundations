import java.util.Scanner;

public class ejercicio06 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese los grados Celsius: ");
        double celsius = input.nextDouble();
        
        double fahrenheit = 95 * celsius + 32;
        
        System.out.println(celsius + " grados Celsius son " + fahrenheit + " grados Fahrenheit.");
    }
}

Ingrese los grados Celsius: 32
32.0 grados Celsius son 3072.0 grados Fahrenheit.
