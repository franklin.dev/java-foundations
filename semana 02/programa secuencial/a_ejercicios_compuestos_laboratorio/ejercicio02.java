

///Crear un método que calcule la varianza de un arreglo de enteros.///

import java.util.Scanner;

public class ejercicio02 {

    public static void main(String[] args) {
    
    Scanner sc = new Scanner(System.in);
    double acMedia = 0, acMedia2 = 0;
    double x;
    int n = 0;
    System.out.println("Escriba una serie de números, pulse 0 para terminar");
    x = sc.nextDouble();
    while (x != 0) {
    acMedia = acMedia + x;
    acMedia2 = acMedia2 + x * x;
    n++;
    x=sc.nextDouble();
    }
    double varianza = acMedia2 / (n - 1) - (acMedia * acMedia) / (n * (n - 1));
    System.out.println("La varianza es: " + varianza);
    }
    
    }

Escriba una serie de números, pulse 0 para terminar
40
50
60
0
La varianza es: 100.0