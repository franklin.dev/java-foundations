///Crear un método que ordene un arreglo de enteros de manera descendente.///

import java.util.Scanner;

public class ejercicio04 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        System.out.print("Manera Ascendente: ");
        
        n = sc.nextInt();
        System.out.println("\nNumeros del 1 al " + n + ": ");                                           
        for (int i = 1; i <= n; i++) {
            System.out.println(i);
        }
    }
}

Manera Ascendente: 10

Numeros del 1 al 10:
1
2
3
4
5
6
7
8
9
10