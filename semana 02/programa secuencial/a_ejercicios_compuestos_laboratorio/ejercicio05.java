
///Crear un método que elimine los elementos duplicados de un arreglo de enteros.///

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
 
public class ejercicio05 {
    public static int[] removeDuplicates(int[] arr) {
        Set<Integer> set = new HashSet<>();
        for (int i : arr) {
            set.add(i);
        }
 
        int[] result = new int[set.size()];
        int k = 0;
        for (int i: set) {
            result[k++] = i;
        }
        return result;
    }
 
    public static void main(String[] args) {
        int[] arr = {2, 4, 1, 2, 1, 2, 4, 5};
 
        int[] distinct = removeDuplicates(arr);
        System.out.println(Arrays.toString(distinct));
    }

}

[1, 2, 4, 5]