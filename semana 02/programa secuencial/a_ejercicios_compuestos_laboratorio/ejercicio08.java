//Leer un numero entre 1000 a 7000 y almacenar los digitos en un array e imprimirlos en pantalla de la siguiente manera:
//Ingresa el número: 5501
//Salida:
//[3] = 5
//[2] = 5
//[1] = 0
//[0] = 1
import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        int numero, i = 0;
        int[] digitos = new int[4];
        Scanner entrada = new Scanner(System.in);

        System.out.print("Ingresa el número: ");
        numero = entrada.nextInt();

        while (numero > 0) {
            digitos[i] = numero % 10;
            numero /= 10;
            i++;
        }

        for (int j = 3; j >= 0; j--) {
            System.out.println("[" + j + "] = " + digitos[j]);
        }
    }
}
Ingresa el número: 5501
[3] = 5
[2] = 5
[1] = 0
[0] = 1
