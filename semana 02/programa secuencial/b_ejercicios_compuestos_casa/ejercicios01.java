public class ejercicios01 {
    public static void main(String[] args) {
        int[][] matrix = new int[7][7];
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = i * matrix.length + j + 1;
            }
        }
        
        System.out.println("Matriz original:");
        printMatrix(matrix);
        
        int[][] rotatedMatrix = rotateMatrix(matrix);
        
        System.out.println("Matriz rotada 90 grados en sentido horario:");
        printMatrix(rotatedMatrix);
    }
    
    public static int[][] rotateMatrix(int[][] matrix) {
        int n = matrix.length;
        int[][] rotatedMatrix = new int[n][n];
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                rotatedMatrix[j][n-1-i] = matrix[i][j];
            }
        }
        
        return rotatedMatrix;
    }
    
    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

Matriz original:
1       2       3       4       5       6       7 
8       9       10      11      12      13      14
15      16      17      18      19      20      21
22      23      24      25      26      27      28
29      30      31      32      33      34      35
36      37      38      39      40      41      42
43      44      45      46      47      48      49
Matriz rotada 90 grados en sentido horario:
43      36      29      22      15      8       1
44      37      30      23      16      9       2
45      38      31      24      17      10      3
46      39      32      25      18      11      4
47      40      33      26      19      12      5
48      41      34      27      20      13      6
49      42      35      28      21      14      7