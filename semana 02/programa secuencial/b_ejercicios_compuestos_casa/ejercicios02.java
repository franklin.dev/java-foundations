import java.util.Random;

public class ejercicios02 {
    public static void main(String[] args) {
        int[][] matrix = new int[7][7];
        
        Random random = new Random();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = random.nextInt(100) + 1;
            }
        }
        
        printMatrix(matrix);
    }
    
    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

20      6       48      21      39      45      90
42      58      100     92      49      2       83
16      86      51      33      80      96      23
27      30      69      2       46      75      73
74      30      24      64      42      50      89
93      43      29      57      74      18      86
67      78      99      85      7       47      96