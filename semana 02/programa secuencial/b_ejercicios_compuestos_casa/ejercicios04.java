import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

public class ejercicios04 {
    public static void main(String[] args) {
        double[][] matrixData = {{1, 2, 0, 0, 0, 0, 0},
                                 {0, 1, 2, 0, 0, 0, 0},
                                 {0, 0, 1, 0, 0, 0, 0},
                                 {0, 0, 0, 2, 0, 0, 0},
                                 {0, 0, 0, 0, 2, 0, 0},
                                 {0, 0, 0, 0, 0, 3, 0},
                                 {0, 0, 0, 0, 0, 0, 3}};
        RealMatrix matrix = MatrixUtils.createRealMatrix(matrixData);

        EigenDecomposition eig = new EigenDecomposition(matrix);
        RealMatrix v = eig.getV();
        RealMatrix d = eig.getD();

        int n = matrix.getRowDimension();
        RealMatrix jordan = MatrixUtils.createRealMatrix(n, n);

        for (int i = 0; i < n; i++) {
            double lambda = d.getEntry(i, i);
            RealMatrix submatrix = v.getSubMatrix(0, n - 1, i, i);
            int m = submatrix.getColumnDimension();

            for (int j = 0; j < m; j++) {
                jordan.setEntry(i, i + j, lambda);
                if (j < m - 1) {
                    jordan.setEntry(i + 1 + j, i + j, 1);
                }
            }
            i += m - 1;
        }

        System.out.println("Forma canónica de Jordan:");
        System.out.println(jordan);
    }
}
