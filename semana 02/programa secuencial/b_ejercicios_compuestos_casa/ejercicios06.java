import java.util.Arrays;
import java.util.Comparator;

public class ejercicios06 {
    public static void main(String[] args) {
        String[] strings = {"banana", "apple", "orange", "pear"};

        Arrays.sort(strings, Comparator.reverseOrder());

        System.out.println(Arrays.toString(strings));
    }
}

[pear, orange, banana, apple]