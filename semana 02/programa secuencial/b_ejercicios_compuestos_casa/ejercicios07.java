import java.util.PriorityQueue;

public class ejercicios07 {
    public static void main(String[] args) {
        int[] nums = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};

        int k = 3;

        PriorityQueue<Integer> pq = new PriorityQueue<>();

        for (int num : nums) {
            pq.offer(num);

            if (pq.size() > k) {
                pq.poll();
            }
        }

        int kthLargest = pq.peek();

        System.out.println("El " + k + "-ésimo elemento más grande es " + kthLargest);
    }
}

El 3-ésimo elemento más grande es 5