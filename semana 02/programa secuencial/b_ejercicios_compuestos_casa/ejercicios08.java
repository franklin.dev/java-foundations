public class ejercicios08 {
    public static void main(String[] args) {
        int[] nums = {2, 3, 5, 7, 11, 13, 17};
        int count = 0;

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int sum = nums[i] + nums[j];

                if (isPrime(sum)) {
                    count++;
                }
            }
        }

        System.out.println("El número de parejas cuya suma es un número primo es: " + count);
    }

    private static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }

        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }

        return true;
    }
}

El número de parejas cuya suma es un número primo es: 4