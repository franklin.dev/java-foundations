import java.util.HashMap;
import java.util.Map;

public class ejercicios10 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 3, 4, 5, 5, 5, 6};
        int mode = findMode(nums);

        System.out.println("La moda del arreglo es: " + mode);
    }

    private static int findMode(int[] nums) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        for (int num : nums) {
            frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1);
        }

        int maxFrequency = 0;
        int mode = 0;
        for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
            int frequency = entry.getValue();
            if (frequency > maxFrequency) {
                maxFrequency = frequency;
                mode = entry.getKey();
            }
        }

        return mode;
    }
}

La moda del arreglo es: 5