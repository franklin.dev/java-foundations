public class ejercicios12 {
    public static void main(String[] args) {
        int[] nums = {5, 3, 10, 2, 8};
        int max = findMax(nums);

        System.out.println("El número más grande es: " + max);
    }

    private static int findMax(int[] nums) {
        int max = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > max) {
                max = nums[i];
            }
        }
        return max;
    }
}

El número más grande es: 10