import java.util.Arrays;

public class ejercicios14 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 3, 3, 4, 5};
        int x = 3;

        Arrays.sort(arr);

        int count = countOccurrences(arr, x);

        System.out.println("El número " + x + " aparece " + count + " veces en el arreglo.");
    }

    private static int countOccurrences(int[] arr, int x) {
        int left = 0;
        int right = arr.length - 1;
        int count = 0;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (arr[mid] == x) {
                count++;
                int i = mid - 1;
                while (i >= left && arr[i] == x) {
                    count++;
                    i--;
                }
                int j = mid + 1;
                while (j <= right && arr[j] == x) {
                    count++;
                    j++;
                }
                return count;
            } else if (arr[mid] < x) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        return count;
    }
}

El número 3 aparece 3 veces en el arreglo.