import java.util.HashMap;

public class ejercicios16 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2, 2, 4, 5, 5, 5, 5};

        int mostFrequent = findMostFrequentNumber(arr);

        System.out.println("El número que se repite más veces es: " + mostFrequent);
    }

    private static int findMostFrequentNumber(int[] arr) {
        HashMap<Integer, Integer> frequencyMap = new HashMap<>();

        for (int i = 0; i < arr.length; i++) {
            if (frequencyMap.containsKey(arr[i])) {
                frequencyMap.put(arr[i], frequencyMap.get(arr[i]) + 1);
            } else {
                frequencyMap.put(arr[i], 1);
            }
        }

        int maxFrequency = 0;
        int mostFrequent = 0;
        for (int key : frequencyMap.keySet()) {
            int frequency = frequencyMap.get(key);
            if (frequency > maxFrequency) {
                maxFrequency = frequency;
                mostFrequent = key;
            }
        }

        return mostFrequent;
    }
}

El número que se repite más veces es: 5
