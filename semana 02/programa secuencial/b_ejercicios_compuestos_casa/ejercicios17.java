int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
int mid = arr.length / 2; // calcular el punto medio del arreglo

int[] arr1 = new int[mid];
int[] arr2 = new int[arr.length - mid];

System.arraycopy(arr, 0, arr1, 0, mid);
System.arraycopy(arr, mid, arr2, 0, arr.length - mid);

System.out.println(Arrays.toString(arr1));
System.out.println(Arrays.toString(arr2));