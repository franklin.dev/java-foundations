int[] array1 = {1, 2, 3};
int[] array2 = {4, 5, 6};

int productoPunto = 0;

for (int i = 0; i < array1.length; i++) {
    productoPunto += array1[i] * array2[i];
}

System.out.println("El producto punto entre los arrays es: " + productoPunto);
