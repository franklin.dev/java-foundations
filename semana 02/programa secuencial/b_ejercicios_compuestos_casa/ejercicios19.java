import java.util.Scanner;

public class ejercicios19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce una letra: ");
        char letra = scanner.nextLine().charAt(0);

        if (esVocal(letra)) {
            System.out.println("La letra " + letra + " es una vocal");
        } else {
            System.out.println("La letra " + letra + " es una consonante");
        }
    }

    public static boolean esVocal(char letra) {
        letra = Character.toLowerCase(letra);

        if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
            return true;
        } else {
            return false;
        }
    }
}

Introduce una letra: o 
La letra o es una vocal
Introduce una letra: m
La letra m es una consonante