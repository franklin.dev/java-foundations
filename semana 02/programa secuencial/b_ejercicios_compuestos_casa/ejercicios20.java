public class ejercicios20 {
    public static void main(String[] args) {
        int[] asciiCodes = new int[128]; 

        for (int i = 0; i < asciiCodes.length; i++) {
            asciiCodes[i] = i;
        }

        for (int code : asciiCodes) {
            System.out.print((char) code + " ");
    }
}

 ☺ ☻ ♥ ♦ ♣ ♠     
 ♫ ☼ ► ◄ ↕ ‼ ¶ § ▬ ↨ ↑ ↓ → ∟↔▲▼ 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \ ] ^ _ ` a b c d e f g h i j k l m n o p q 
r s t u v w x y z { | } ~ ⌂
