import java.util.Scanner;

public class ejercicios02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingresa la cantidad de numeros: ");
        int n = sc.nextInt();

        int[] numeros = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingresa el numero " + (i + 1) + ": ");
            numeros[i] = sc.nextInt();
        }

        int mcd = calcularMCD(numeros);

        System.out.println("El Máximo Común Divisor es: " + mcd);
    }

    public static int calcularMCD(int[] numeros) {
        int mcd = numeros[0];

        int i = 1;
        while (i < numeros.length) {
            mcd = calcularMCD(mcd, numeros[i]);
            i++;
        }

        return mcd;
    }

    public static int calcularMCD(int a, int b) {
        while (b > 0) {
            int resto = a % b;
            a = b;
            b = resto;
        }
        return a;
    }
}

Ingresa la cantidad de numeros: 3
Ingresa el numero 1: 5
Ingresa el numero 2: 10
Ingresa el numero 3: 8
El Máximo Común Divisor es: 1

