public class ejercicio03 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: java Factorial <number>");
            System.exit(1);
        }
        int n = Integer.parseInt(args[0]);
        if (n < 0) {
            System.err.println("Error: number must be non-negative");
            System.exit(1);
        }
        int factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        System.out.println(n + "! = " + factorial);
    }
}

