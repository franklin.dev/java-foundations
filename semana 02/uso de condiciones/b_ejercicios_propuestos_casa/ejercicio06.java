import java.util.Scanner;

public class ejercicio06{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de números a ordenar: ");
        int n = scanner.nextInt();

        int[] nums = new int[n];

        System.out.println("Ingrese los números a ordenar:");
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }

        for (int i = 1; i < n; i++) {
            int key = nums[i];
            int j = i - 1;
            while (j >= 0 && nums[j] > key) {
                nums[j + 1] = nums[j];
                j--;
            }
            nums[j + 1] = key;
        }

        System.out.println("Los números ordenados son:");
        for (int num : nums) {
            System.out.print(num + " ");
        }
        System.out.println();
    }
}
