import java.util.Scanner;

public class ejercicio07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la cantidad de términos de la sucesión de Ulam a imprimir: ");
        int n = scanner.nextInt();

        int[] ulam = new int[n];
        ulam[0] = 1;
        ulam[1] = 2;

        for (int i = 2; i < n; i++) {
            int sum = ulam[i - 1] + ulam[i - 2];
            boolean isUlam = true;
            for (int j = 0; j < i && isUlam; j++) {
                for (int k = j + 1; k < i && isUlam; k++) {
                    if (ulam[j] + ulam[k] == sum) {
                        isUlam = false;
                    }
                }
            }
            if (isUlam) {
                ulam[i] = sum;
            } else {
                i--;
            }
        }

        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int i = 0; i < n; i++) {
            System.out.print(ulam[i] + " ");
        }
        System.out.println();
    }
}
