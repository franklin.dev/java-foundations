import java.util.Scanner;

public class ejercicio08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese un número entero positivo: ");
        int num = scanner.nextInt();

        int sum = 0;
        int n = num;
        while (n > 0) {
            sum += n % 10;
            n /= 10;
        }

        if (num % sum == 0) {
            System.out.println(num + " es un número de Harshad.");
        } else {
            System.out.println(num + " no es un número de Harshad.");
        }
    }
}
