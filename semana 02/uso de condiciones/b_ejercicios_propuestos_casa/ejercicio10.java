import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class ejercicio10 {
    
    public static void main(String[] args) {
        int[] data = readDataFromFile("datos.txt");
        System.out.println("Datos: " + Arrays.toString(data));
        
        double mean = calculateMean(data);
        System.out.println("Promedio: " + mean);
        
        int mode = calculateMode(data);
        System.out.println("Moda: " + mode);
        
        double stdev = calculateStandardDeviation(data);
        System.out.println("Desviación estándar: " + stdev);
    }
    
    public static int[] readDataFromFile(String filename) {
        int[] data = new int[100];
        int count = 0;
        try {
            File file = new File(filename);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextInt()) {
                int number = scanner.nextInt();
                data[count++] = number;
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Arrays.copyOf(data, count);
    }
    
    public static double calculateMean(int[] data) {
        int sum = 0;
        for (int i = 0; i < data.length; i++) {
            sum += data[i];
        }
        return (double) sum / data.length;
    }
    
    public static int calculateMode(int[] data) {
        Arrays.sort(data);
        int mode = data[0];
        int maxCount = 1;
        int count = 1;
        for (int i = 1; i < data.length; i++) {
            if (data[i] == data[i-1]) {
                count++;
            } else {
                if (count > maxCount) {
                    maxCount = count;
                    mode = data[i-1];
                }
                count = 1;
            }
        }
        if (count > maxCount) {
            mode = data[data.length-1];
        }
        return mode;
    }
    
    public static double calculateStandardDeviation(int[] data) {
        double mean = calculateMean(data);
        double sum = 0;
        for (int i = 0; i < data.length; i++) {
            sum += Math.pow(data[i] - mean, 2);
        }
        return Math.sqrt(sum / data.length);
    }
}
