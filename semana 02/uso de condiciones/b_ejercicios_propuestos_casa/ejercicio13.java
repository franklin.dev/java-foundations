import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa el número de segundos: ");
        int segundos = sc.nextInt();
        sc.close();
        for (int i = segundos; i >= 0; i--) {
            System.out.print(i + " ");
            try {
                Thread.sleep(1000); 
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n¡Tiempo terminado!");
    }
}
