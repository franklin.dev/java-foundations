import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int n = scanner.nextInt();
        scanner.close();

        if (n < 1) {
            System.out.println("El número ingresado no es positivo.");
            return;
        }

        System.out.print("Números impares desde 1 hasta " + n + ": ");
        for (int i = 1; i <= n; i += 2) {
            System.out.print(i);
            if (i < n - 1) {
                System.out.print("; ");
            } else if (i == n - 1) {
                System.out.print(" y ");
            } else {
                System.out.print(".");
            }
        }
    }
}
