public class ejercicio15 {
    public static void main(String[] args) {
        double capital = 5000;
        double tasaInteresMensual = 0.016;
        int plazoMeses = 18;

        double saldoPendiente = capital;
        for (int mes = 1; mes <= plazoMeses; mes++) {
            double pagoMensual = capital * tasaInteresMensual * Math.pow(1 + tasaInteresMensual, plazoMeses)
                    / (Math.pow(1 + tasaInteresMensual, plazoMeses) - 1);
            double interesMensual = saldoPendiente * tasaInteresMensual;
            double amortizacionMensual = pagoMensual - interesMensual;
            saldoPendiente -= amortizacionMensual;

            System.out.printf("Mes %d: Saldo pendiente = %.2f\n", mes, saldoPendiente);
        }
    }
}
