import java.util.Scanner;

public class ejercicio16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero entero: ");
        int n = sc.nextInt();
        
        for (int i = 1; i <= n; i++) {
            for (int j = i*2-1; j >= 1; j -= 2) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}

1 
3 1 
5 3 1 
7 5 3 1 
9 7 5 3 1 
