import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese el tamaño del vector: ");
        int n = sc.nextInt();

        int[] vector = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el valor para la posición " + i + ": ");
            vector[i] = sc.nextInt();
        }

        int maximo = vector[0];
        int minimo = vector[0];
        for (int i = 1; i < n; i++) {
            if (vector[i] > maximo) {
                maximo = vector[i];
            }
            if (vector[i] < minimo) {
                minimo = vector[i];
            }
        }

        // Mostramos el resultado
        System.out.println("El valor máximo del vector es: " + maximo);
        System.out.println("El valor mínimo del vector es: " + minimo);

        sc.close();
    }
}
