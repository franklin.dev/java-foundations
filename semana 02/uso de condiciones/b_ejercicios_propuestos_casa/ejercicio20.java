import java.util.Scanner;

public class ejercicio20 {
    
    public static boolean esPerfecto(int num) {
        int suma = 0;
        for (int i = 1; i <= num/2; i++) {
            if (num % i == 0) {
                suma += i;
            }
        }
        return (suma == num);
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números perfectos que desea sumar: ");
        int n = sc.nextInt();
        
        int numPerfectosEncontrados = 0;
        int suma = 0;
        int i = 1;
        while (numPerfectosEncontrados < n) {
            if (esPerfecto(i)) {
                numPerfectosEncontrados++;
                suma += i;
            }
            i++;
        }
        
        System.out.println("La suma de los primeros " + n + " números perfectos es: " + suma);
    }
}
