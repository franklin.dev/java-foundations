public class ejercicios1 {
    public static void main(String[] args) {
        int base = 5;
        int exponente = 3;
        double resultado = Math.pow(base, exponente);
        System.out.println(base + " elevado a la " + exponente + " es igual a " + resultado);
    }
}

5 elevado a la 3 es igual a 125.0