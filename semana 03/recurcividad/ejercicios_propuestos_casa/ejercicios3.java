public class ejercicios3 {
    public static void main(String[] args) {
        int m = 2, n = 3;
        int result = ackermann(m, n);

        System.out.println("El término " + result + " de la secuencia de Ackermann con m = " + m + " y n = " + n + ".");
    }

    private static int ackermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (n == 0) {
            return ackermann(m - 1, 1);
        } else {
            return ackermann(m - 1, ackermann(m, n - 1));
        }
    }
}

El término 9 de la secuencia de Ackermann con m = 2 y n = 3.